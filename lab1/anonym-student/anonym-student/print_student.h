#ifndef PRINT_STUDENT_H
#define PRINT_STUDENT_H

#include "student_struct.h"

#include <iostream>

void printStudent(const Student& student) {
  std::cout << "Student -"
            << " organization: " << student.organization
            << " name: " << student.name << " faculty: " << student.faculty
            << " rating: " << student.rating << std::endl;
}

#endif  // PRINT_STUDENT_H
