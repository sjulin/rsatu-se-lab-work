#include "anonymize.h"
#include "print_student.h"

int main() {
  Student student;
  student.name = "Ivan";
  student.rating = 100;
  student.faculty = "FREI";
  student.organization = "RSATU";

  printStudent(student);

  anonymize(student);

  printStudent(student);
  return 0;
}
