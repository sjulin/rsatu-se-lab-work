#ifndef STUDENT_STRUCT_H
#define STUDENT_STRUCT_H

#include <string>

struct Student {
  std::string organization;
  std::string name;
  std::string faculty;
  int rating;
};

#endif  // STUDENT_STRUCT_H
