#ifndef ANONYMIZE_H
#define ANONYMIZE_H

#include "student_struct.h"

void anonymize(Student& student) {
  student.name = "None";
}

#endif  // ANONYMIZE_H
