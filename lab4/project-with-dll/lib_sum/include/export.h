#ifndef LIB_SUM_EXPORT_H
#define LIB_SUM_EXPORT_H

#if defined LIB_SUM_DLL
    #define LIB_SUM_EXPORT __declspec(dllexport)
#else
    #define LIB_SUM_EXPORT __declspec(dllimport)
#endif

#endif // LIB_SUM_EXPORT_H
