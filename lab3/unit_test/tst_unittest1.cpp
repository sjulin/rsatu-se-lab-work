#include <QtTest>

// add necessary includes here
#include "../project/utils.h"

class UnitTest1 : public QObject
{
    Q_OBJECT

public:
    UnitTest1();
    ~UnitTest1();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();
    void test_case2();
    void test_case3();

private:
    int* m_array = nullptr;
    std::vector<int> m_vector;

};



UnitTest1::UnitTest1()
{

}

UnitTest1::~UnitTest1()
{

}

void UnitTest1::initTestCase()
{
    m_array = new int[2];
    m_array[0] = 1;
    m_array[1] = 2;
}

void UnitTest1::cleanupTestCase()
{
    delete m_array;
}

void UnitTest1::test_case1()
{
    QCOMPARE(sum(12,3) , 15);
}

void UnitTest1::test_case2()
{
    QCOMPARE(sum(m_array[1],4) , 6);
}

void UnitTest1::test_case3()
{
    QCOMPARE(divF(9, 3), 3);
    QVERIFY_EXCEPTION_THROWN(divF(9,0), std::exception);
}

QTEST_APPLESS_MAIN(UnitTest1)

#include "tst_unittest1.moc"
