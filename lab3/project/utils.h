#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <string>
#include <algorithm>

int sum(int a, int b)
{
    return a + b;
}

int divF(int a, int b)
{
    if(b == 0)
        throw std::exception();
    return a / b;
}

std::vector<int> sort(std::vector<int> const& vector)
{
    auto result = vector;
    std::sort(result.begin(), result.end());
    return result;
}

std::string firstWord(std::string const& str)
{
    std::string::size_type pos = str.find(" ");
    if(pos == std::string::npos)
        return str;
    std::string result = str.substr(0, pos );
    if(result.size() <= 0)
        return "";
    bool isDigitString = false;
    for(int i = 0; i < result.size(); ++i)
    {
        if(result[i] == '0' ||  result[i] == '1' || result[i] =='2' || result[i] == '3' ||
                result[i] == '4' || result[i] == '5' || result[i] == '6' || result[i] == '7' ||
                result[i] == '8' || result[i] == '9')
        {
            isDigitString = true;
        }
        else
        {
            isDigitString = false;
            break;
        }
    }
    if(isDigitString)
        return "";
    return result;
}

bool isEmail(std::string const& str)
{
    std::string::size_type posSpace = str.find(" ");
    if(posSpace != std::string::npos)
        return false;
    std::string::size_type posS = str.find("@");
    if(posS == std::string::npos)
        return false;
    std::string::size_type posSSecond = str.find("@", posS + 1);
    if(posSSecond != std::string::npos)
        return false;
    std::string::size_type posDotFirst = str.find(".", posS + 1);
    if(posDotFirst == std::string::npos)
        return false;
    std::string::size_type posDotSecond = str.find(".", posDotFirst + 1);
    if(posDotSecond != std::string::npos)
        return false;
    return true;
}



#endif // UTILS_H
